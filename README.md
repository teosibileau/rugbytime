# RugbyTime

Simple [scrapy](https://scrapy.org/) spider to grab rugby match torrents

## Dependencies

+ [Ahoy](https://github.com/ahoy-cli/ahoy)
+ [Docker](https://www.docker.com/)

If you don't want to run this as a docker container, the spider code is in the **rugbytime** folder.

## Build the container and run the scraper

```
ahoy docker up
ahoy scrap
```

## Overriding config

There's an example provided in `docker-compose.ci.yml`. Copy the file:

```
cp docker-compose.ci.yml docker-compose.local.yml
```

and edit `docker-compose.local.yml` with your customs.