FROM python:3.6-jessie
MAINTAINER Teo Sibileau

# Update packages and install software
RUN apt-get update \
    && apt-get -y install cron \
                          git \
                          python3-dev \
                          python3-pip \
                          libfontconfig \
                          libssl-dev \
                          build-essential \
                          libffi-dev \
                          software-properties-common

EXPOSE 6800

RUN pip3 install --upgrade pip

ARG ENV=PROD

RUN echo "Building image for $ENV environment"

WORKDIR /code
COPY requirements.txt /code
COPY requirements.dev.txt /code
COPY .docker/install_dev_requirements.sh /code

RUN pip3 install -r requirements.txt
RUN chmod +x install_dev_requirements.sh && ./install_dev_requirements.sh


ADD . /code
ADD ./config /config

ENV DATA_FOLDER='/data'

CMD .docker/run_crawler.sh
