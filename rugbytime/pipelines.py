# -*- coding: utf-8 -*-
import logging

import transmissionrpc

from .settings import SETTINGS

from tinydb import TinyDB, Query


class TinyDBPipeline(object):
    def __init__(self):
        self.files = {}
        self.db = TinyDB(SETTINGS['db'])
        self.tc = None
        if SETTINGS['transmission'].get('queue_torrents', False):
            kwargs = {
                'port': SETTINGS['transmission']['port']
            }
            if SETTINGS['transmission']['user']:
                kwargs['user'] = SETTINGS['transmission']['user']
                kwargs['password'] = SETTINGS['transmission']['password']
            self.tc = transmissionrpc.Client(SETTINGS['transmission']['host'],
                                             **kwargs)

    def process_item(self, item, spider):
        Match = Query()
        match = self.db.search(Match.torrent == item['torrent'])

        if not match:
            doc_id = self.db.insert(item)
            match = self.db.get(doc_id=doc_id)
        else:
            match = match[0]

        if self.tc and not match['queued']:
            t = self.tc.add_torrent(
                match['torrent'],
                download_dir=match['download_dir']
            )
            logging.debug('torrent queued: %s', t)
            self.db.update({'queued': True}, doc_ids=[match.doc_id])
