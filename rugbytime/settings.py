import os
import json
import logging


SETTINGS = {
    'db': os.environ.get('TINYDB_PATH', 'config/db.json'),
    'transmission': {
        'queue_torrents': bool(int(os.environ.get('QUEUE_TORRENTS', '0'))),
        'host': os.environ.get('TRANSMISSION_HOST', False),
        'port': int(os.environ.get('TRANSMISSION_PORT', '9091')),
        'user': os.environ.get('TRANSMISSION_USER', False),
        'password': os.environ.get('TRANSMISSION_PASSWORD', False),
    },
    'pages': int(os.environ.get('PAGES', '4')),
}

logging.debug(json.dumps(SETTINGS))

BOT_NAME = 'rugbytime'

SPIDER_MODULES = ['rugbytime.spiders']
RUGBYTIME_MODULE = 'rugbytime.spiders'

ROBOTSTXT_OBEY = False
ITEM_PIPELINES = {
    'rugbytime.pipelines.TinyDBPipeline': 300,
}

FEED_EXPORTERS_BASE = {
    'xml': 'scrapy.contrib.exporter.XmlItemExporter',
}

HTTPCACHE_ENABLED = True
