#!/usr/bin/env python

import os
import logging
import urllib.parse

from slugify import slugify
from pyquery import PyQuery as pq
from datetime import datetime

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from .settings import SETTINGS
from rugbytime.items import MatchTorrent


class RugbyTimeSpider(CrawlSpider):
    name = 'rugbytime'
    allowed_domains = ["www.sport-video.org.ua"]

    start_urls = ['https://www.sport-video.org.ua/rugby.html']

    for i in range(1, SETTINGS.get('pages', 3)):
        start_urls.append('https://www.sport-video.org.ua/rugby%s.html' % i)

    rules = (
        Rule(
            LinkExtractor(
                allow='www.sport-video.org.ua/[A-Z]+[0-9]{6}.html$',
            ),
            'parse_match', follow=True
        ),
    )

    def parse_match(self, response):
        match = MatchTorrent()
        d = pq(response.body)
        title = d("#wb_Text11 span strong").text()
        description = d('#Table2 tr:first td:last').text()
        torrent = d('#wb_Shape1 a').attr('href').replace('./', '')
        torrent = 'http://www.sport-video.org.ua/%s' % urllib.parse.quote(torrent)

        date = title[-10:]
        teams = title.replace(date, '')

        date = date.replace('/', '.')
        date = datetime.strptime(date, '%d.%M.%Y')

        teams = teams.split('-')
        teams = [team.strip() for team in teams]

        tournament = slugify(description.lower())

        data_folder = os.environ.get('DATA_FOLDER', '/data')

        folder = '%s/rugbytime/%s' % (data_folder,
                                      tournament)

        match['date'] = date.strftime('%Y/%m/%d')
        match['teams'] = teams
        match['description'] = description
        match['tournament'] = tournament
        match['download_dir'] = folder
        match['torrent'] = torrent
        match['queued'] = False

        logging.debug('%s', match)
        return match
