import scrapy
from scrapy.item import Field


class MatchTorrent(scrapy.Item):
    date = Field()
    teams = Field()
    torrent = Field()
    description = Field()
    tournament = Field()
    download_dir = Field()
    queued = Field()
